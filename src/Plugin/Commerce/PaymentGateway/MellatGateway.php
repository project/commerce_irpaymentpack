<?php

namespace Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_irpaymentpack\Banks\MellatBank;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The commerce payment gateway plugin for MellatBank
 *
 * @CommercePaymentGateway(
 *   id = "commerce_irpaymentpack_mellat",
 *   label = @Translation("IRPaymentPack: Mellat"),
 *   display_label = @Translation("Mellat Bank"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect\MellatRedirect",
 *   },
 *   modes = {"Live"},
 *   payment_method_types = {"credit_card"},
 * )
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class MellatGateway extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['mellat_terminal_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terminal ID'),
      '#default_value' => $this->configuration['mellat_terminal_id'] ?? '',
      '#description' => $this->t('The terminal code that you have got from Bank.'),
      '#required' => TRUE,
    ];

    $form['mellat_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $this->configuration['mellat_username'] ?? '',
      '#required' => TRUE,
      '#description' => $this->t('Mellat gateway username.'),
    ];

    $form['mellat_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $this->configuration['mellat_password'] ?? '',
      '#required' => TRUE,
      '#description' => $this->t('Mellat gateway password.'),
    ];

    $form['mellat_order_base_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Base Order ID'),
      '#default_value' => $this->configuration['mellat_order_base_id'] ?? 0,
      '#required' => TRUE,
      '#min' => 0,
      '#description' => $this->t('This number will be added to your shop\'s order id, to create the @bank remote Order ID.' .
        ' Note that the @bank requires a forever-unique order ID per gateway.', ['@bank' => $this->t('MellatBank')]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      // Save configuration
      $this->configuration['mellat_terminal_id']   = $values['mellat_terminal_id'];
      $this->configuration['mellat_username']      = $values['mellat_username'];
      $this->configuration['mellat_password']      = $values['mellat_password'];
      $this->configuration['mellat_order_base_id'] = $values['mellat_order_base_id'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $terminal_id   = $this->configuration['mellat_terminal_id']   ?? '';
    $username      = $this->configuration['mellat_username']      ?? '';
    $password      = $this->configuration['mellat_password']      ?? '';
    $order_base_id = $this->configuration['mellat_order_base_id'] ?? 0;

    // If the configuration parameters are empty, display an error.
    if (empty($terminal_id) || empty($username) || empty($password)) {
      $this->messenger()->addError($this->t('Bank is not configured properly. Please contact site administrator.'));

      throw new InvalidRequestException($this->t("Bank is not configured properly. Please contact site administrator."));
    }

    // calculate the remote order id. MellatBank always requires a unique ID (forever-unique per gateway)
    $remote_order_id = $order_base_id + $order->id();

    $mellat = new MellatBank($terminal_id, $username, $password, $remote_order_id);

    // Fetch callback parameters
    $params = $mellat->getCallBackParameters();

    if ($params['resCode'] !== '0') {
      // Error in gateway (e.g. user cancelled transaction or etc...)
      // We don't need verification
      $msg = $mellat->errorTranslate($params['resCode']);
      $this->messenger()->addError($this->t($msg));
      throw new PaymentGatewayException($msg);
    }

    // load the previously saved payment
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->loadByProperties([
      'order_id'     => $order->id(),
      'remote_state' => $params['refId'],
      'state'        => 'authorization',
    ]);
    // The user may have a few payments (i.e. previous payment tries have been failed)
    $payment = end($payment);
    if (!$payment) {
      // Could not find the payment.
      throw new InvalidRequestException($this->t('Could not find the payment record.'));
    }

    // Verify the request
    $success = $mellat->verifyRequest($params['saleOrderId'], $params['saleReferenceId']);
    if ($success) {
      $payment->setState('completed');
      $payment->setRemoteId($request->request->get('SaleReferenceId'));
      $payment->save();

      $this->messenger()->addStatus($this->t('Payment was successful.'));

      // TODO: Is it necessary to call MellatBank's bpSettleRequest() at this point?
    }
    else {
      // Payment verification failed.
      $error = $mellat->getError();
      $msg = t('Verification failed.');

      if (is_int($error['object']['verify_result'])) {
        $msg .= ' ' . t($mellat->errorTranslate($error['object']['verify_result']));
      }

      $this->messenger()->addError($msg);
      \Drupal::logger('commerce_irpaymentpack')->warning($msg . " \n\n" . print_r($error['object'], TRUE));

      throw new PaymentGatewayException($msg);
    }
  }

}
