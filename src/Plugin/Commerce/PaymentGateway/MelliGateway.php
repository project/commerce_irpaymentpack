<?php

namespace Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_irpaymentpack\Banks\MelliBank;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The commerce payment gateway plugin for MelliBank
 *
 * @CommercePaymentGateway(
 *   id = "commerce_irpaymentpack_melli",
 *   label = @Translation("IRPaymentPack: Melli (Sadad)"),
 *   display_label = @Translation("Melli Bank"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect\MelliRedirect",
 *   },
 *   modes = {"Live"},
 *   payment_method_types = {"credit_card"},
 * )
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class MelliGateway extends OffsitePaymentGatewayBase {

  /**
   *
   */
  public const VERIFY_URL = 'https://sadad.shaparak.ir/api/v0/Advice/Verify';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['melli_merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['melli_merchant_id'] ?? '',
      '#description' => $this->t('The Merchant ID that you\'ve got from the Bank.'),
      '#required' => TRUE,
    ];

    $form['melli_terminal_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terminal ID'),
      '#default_value' => $this->configuration['melli_terminal_id'] ?? '',
      '#description' => $this->t('The Terminal ID that you\'ve got from the Bank.'),
      '#required' => TRUE,
    ];

    $form['melli_terminal_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terminal Key (*** KEEP SECURE ***)'),
      '#default_value' => $this->configuration['melli_terminal_key'] ?? '',
      '#required' => TRUE,
      '#description' => $this->t('The TerminalKey provided by MelliBank.'),
    ];

    $form['melli_order_base_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Base Order ID'),
      '#default_value' => $this->configuration['melli_order_base_id'] ?? 0,
      '#required' => TRUE,
      '#min' => 0,
      '#description' => $this->t('This number will be added to your shop\'s order id, to create the @bank remote Order ID.' .
        ' Note that the @bank requires a forever-unique order ID per gateway.', ['@bank' => $this->t('MelliBank')]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      // Save configuration
      $this->configuration['melli_merchant_id']   = $values['melli_merchant_id'];
      $this->configuration['melli_terminal_id']   = $values['melli_terminal_id'];
      $this->configuration['melli_order_base_id'] = $values['melli_order_base_id'];
      $this->configuration['melli_terminal_key']  = $values['melli_terminal_key'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $merchant_id   = $this->configuration['melli_merchant_id']   ?? '';
    $terminal_id   = $this->configuration['melli_terminal_id']   ?? '';
    $terminal_key  = $this->configuration['melli_terminal_key']  ?? '';

    // If the configuration parameters are empty, display an error.
    if (empty($merchant_id) || empty($terminal_id) || empty($terminal_key)) {
      $this->messenger()->addError($this->t('Bank is not configured properly. Please contact site administrator.'));

      throw new InvalidRequestException($this->t("Bank is not configured properly. Please contact site administrator."));
    }

    $token    = $request->request->get('token');
    $res_code = $request->request->get('ResCode');

    if ($res_code == 0) {
      // load the previously saved payment
      $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
      $payment = $payment_storage->loadByProperties([
        'order_id'     => $order->id(),
        'state'        => 'authorization',
      ]);
      // The user may have a few payments (i.e. previous payment tries have been failed)
      /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
      $payment = end($payment);
      if (!$payment) {
        // Could not find the payment.
        throw new InvalidRequestException($this->t('Could not find the payment record.'));
      }

      // verify the transaction
      $verify_data = [
        'Token'    => $token,
        'SignData' => MelliBank::encryptPKCS7($token, $terminal_key),
      ];
      $result = MelliBank::callAPI(self::VERIFY_URL, $verify_data);

      $verify_ResCode = $result['ResCode'];
      if ($verify_ResCode == 0) {
        // success
        $systemTraceNo  = $result['SystemTraceNo'];
        $retrievalRefNo = $result['RetrivalRefNo']; // spell is correct (per Bank's document)

        $payment->setState('completed');
        $payment->setRemoteId($retrievalRefNo);
        // TODO: Should we also save $systemTraceNo somewhere in database?
        $payment->save();

        $this->messenger()->addStatus($this->t('Payment was successful. Your tracking code: @code', [
          '@code' => $systemTraceNo
        ]));
      }
      else {
        // verify failed.
        // TODO: translate $verify_ResCode (either 0, -1 or 101)
        $msg = $this->t('Verification failed. Code: @code', ['@code' => $verify_ResCode]);
        $this->messenger()->addError($msg);

        throw new PaymentGatewayException($msg);
      }
    }
    else {
      // Error in gateway (e.g. user cancelled transaction or etc...)
      // We don't need verification
      $msg = $this->t('Error in gateway. Transaction failed.');
      $this->messenger()->addError($msg);

      throw new PaymentGatewayException($msg);
    }
  }

}
