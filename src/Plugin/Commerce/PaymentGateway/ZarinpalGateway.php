<?php

namespace Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_irpaymentpack\Banks\Zarinpal;
use Drupal\commerce_irpaymentpack\Banks\ZarinpalException;
use Drupal\commerce_irpaymentpack\Banks\ZarinpalExceptionNok;
use Drupal\commerce_irpaymentpack\Banks\ZarinpalExceptionVerification;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The commerce payment gateway plugin for Zarinpal
 *
 * @CommercePaymentGateway(
 *   id = "commerce_irpaymentpack_zarinpal",
 *   label = @Translation("IRPaymentPack: Zarinpal"),
 *   display_label = @Translation("Zarinpal"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect\ZarinpalRedirect",
 *   },
 *   payment_method_types = {"credit_card"},
 * )
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class ZarinpalGateway extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['zarinapl_merchant_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Code'),
      '#default_value' => $this->configuration['zarinapl_merchant_code'] ?? '',
      '#description' => $this->t('The merchant code is provided by Zarinpal. In the Test mode, any arbitrary code such as 123 can be used.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      // Save configuration
      $this->configuration['zarinapl_merchant_code'] = $values['zarinapl_merchant_code'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $merchant_code = $this->configuration['zarinapl_merchant_code'] ?? '';

    // If the configuration parameters are empty, display an error.
    if (empty($merchant_code)) {
      $this->messenger()->addError($this->t('Zarinapl is not configured properly. Please contact site administrator.'));

      throw new InvalidRequestException($this->t("Zarinapl is not configured properly. Please contact site administrator."));
    }

    // get amount
    $amount_toman = (int)$order->getTotalPrice()->getNumber();
    // convert Rial to Toman
    if ($order->getTotalPrice()->getCurrencyCode() == 'IRR') {
      $amount_toman /= 10;
    }

    // load the previously saved payment
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->loadByProperties([
      'order_id'     => $order->id(),
      'state'        => 'authorization',
    ]);
    // The user may have a few payments (i.e. previous payment tries have been failed)
    $payment = end($payment);
    if (!$payment) {
      // Could not find the payment.
      throw new InvalidRequestException($this->t('Could not find the payment record.'));
    }

    try {
      $zarinpal = new Zarinpal($merchant_code, $amount_toman);
      $zarinpal->setSandbox($this->configuration['mode'] == 'test');
      $refId = $zarinpal->verifyTransaction();

      $payment->setState('completed');
      $payment->setRemoteId($refId);
      $payment->save();
      $this->messenger()->addStatus($this->t('Payment was successful.'));
    }
    catch (ZarinpalException $e) {
      $msg = 'No valid authority sent by zarinpal. Payment failed.';
      $this->messenger()->addError($this->t($msg));
      throw new PaymentGatewayException($msg);
    }
    catch (ZarinpalExceptionNok $e) {
      $msg = 'Transaction not successful or cancelled by user.';
      $this->messenger()->addError($this->t($msg));
      throw new PaymentGatewayException($msg);
    }
    catch (ZarinpalExceptionVerification $e) {
      // Verification failed.
      watchdog_exception('commerce_irpaymentpack', $e);
      $this->messenger()->addError($e->getMessage());
      throw new PaymentGatewayException($e->getMessage());
    }
    catch (\Exception $e) {
      watchdog_exception('commerce_irpaymentpack', $e);
      $msg = 'An unexpected error occurred.';
      $this->messenger()->addError($this->t($msg));
      throw new PaymentGatewayException($msg);
    }
  }
}
