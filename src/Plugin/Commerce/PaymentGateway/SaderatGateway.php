<?php

namespace Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The commerce payment gateway plugin for Saderat Bank
 *
 * @CommercePaymentGateway(
 *   id = "commerce_irpaymentpack_saderat",
 *   label = @Translation("IRPaymentPack: Saderat"),
 *   display_label = @Translation("Saderat Bank"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect\SaderatRedirect",
 *   },
 *   modes = {"Live"},
 *   payment_method_types = {"credit_card"},
 * )
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class SaderatGateway extends OffsitePaymentGatewayBase {

  /**
   * Rest endpoint URL for the Advice service
   */
  protected const ADVICE_REST_ENDPOINT = 'https://sepehr.shaparak.ir:8081/V1/PeymentApi/Advice';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['saderat_terminal_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terminal ID'),
      '#default_value' => $this->configuration['saderat_terminal_id'] ?? '',
      '#description' => $this->t('The terminal code that you have got from Bank.'),
      '#required' => TRUE,
    ];

    $form['saderat_order_base_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Base Order ID'),
      '#default_value' => $this->configuration['saderat_order_base_id'] ?? 0,
      '#required' => TRUE,
      '#min' => 0,
      '#description' => $this->t('This number will be added to your shop\'s order id, to create the @bank remote Order ID.' .
        ' Note that the @bank requires a forever-unique order ID per gateway.', ['@bank' => $this->t('SaderatBank')]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      // Save configuration
      $this->configuration['saderat_terminal_id']   = $values['saderat_terminal_id'];
      $this->configuration['saderat_order_base_id'] = $values['saderat_order_base_id'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $terminal_id = $this->configuration['saderat_terminal_id'] ?? '';
    $order_base_id = $this->configuration['saderat_order_base_id'] ?? 0;

    // If the configuration parameters are empty, display an error.
    if (empty($terminal_id)) {
      $this->messenger()
        ->addError($this->t('Bank is not configured properly. Please contact site administrator.'));

      throw new InvalidRequestException($this->t("Bank is not configured properly. Please contact site administrator."));
    }

    // calculate the remote order id. Saderat Bank always requires a unique ID (forever-unique per gateway)
    $remote_order_id = $order_base_id + $order->id();

    // get amount
    $amount_rials = (int) $order->getTotalPrice()->getNumber();
    // convert Toman to Rials
    if ($order->getTotalPrice()->getCurrencyCode() == 'TMN') {
      $amount_rials *= 10;
    }

    $invoice_number = $request->request->get('invoiceid');
    $digital_receipt = $request->request->get('digitalreceipt');
    $response_code = $request->request->get('respcode');
    $rrn = $request->request->get('rrn');

    if ($response_code == '0') {
      // load the previously saved payment
      $payment_storage = \Drupal::entityTypeManager()
        ->getStorage('commerce_payment');
      $payment = $payment_storage->loadByProperties([
        'order_id' => $order->id(),
        'state' => 'authorization',
      ]);
      // The user may have a few payments (i.e. previous payment tries have been failed)
      $payment = end($payment);
      if (!$payment) {
        // Could not find the payment.
        throw new InvalidRequestException($this->t('Could not find the payment record.'));
      }

      $dataQuery = "digitalreceipt={$digital_receipt}&Tid={$terminal_id}";

      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, self::ADVICE_REST_ENDPOINT);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $dataQuery);
      $result = curl_exec($curl);
      $curl_err = curl_error($curl);
      curl_close($curl);

      if (empty($result)) {
        $msg = $this->t('Error: Could not connect to the Bank for verifying the transaction. Error: @err', ['@err' => $curl_err]);
        $this->messenger()->addError($msg);
        throw new PaymentGatewayException($msg);
      }

      $result = Json::decode($result);

      if (strtoupper($result['Status']) == 'OK') {
        $paid_amount = $result['ReturnId'] ?? 0;
        if (floatval($paid_amount) == floatval($amount_rials)) {
          $payment->setState('completed');
          $payment->setRemoteId($digital_receipt);
          $payment->save();

          $this->messenger()
            ->addStatus($this->t('Payment was successful. Tracking code: @code', ['@code' => $rrn]));
        }
        else {
          $msg = $this->t('Your paid amount is not equal to the order amount. Paid amount: @paid', ['@paid' => $paid_amount]);
          $this->messenger()->addError($msg);
          throw new PaymentGatewayException($msg);
        }
      }
      else {
        $msg = self::translateErrorCode($result['ReturnId'] ?? 'N/A');

        $this->messenger()->addError($msg);
        throw new PaymentGatewayException($msg);
      }
    }
    else {
      // Error in gateway (e.g. user cancelled transaction or etc...)
      // We don't need verification
      $msg = $request->request->get('error_message') ?? $this->t('Error in gateway.');
      $this->messenger()->addError($msg);
      throw new PaymentGatewayException($msg);
    }
  }

  /**
   * Translate error codes from the Advice service
   */
  public static function translateErrorCode($code) {
    switch ($code) {
      case '-1' :
        $msg = t('Transaction not found.');
        break;
      case '-2' :
        $msg = t('Transaction has been reversed previously.');
        break;
      case '-3' :
        $msg = t('General Error');
        break;
      case '-4' :
        $msg = t('Not possible to send request for this transaction.');
        break;
      case '-5' :
        $msg = t('The IP address of the shop is invalid.');
        break;
      default   :
        $msg = t('Unknown Error: @code', ['@code' => $code]);
        break;
    }

    return $msg;
  }
}
