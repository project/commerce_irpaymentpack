<?php

namespace Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The commerce payment gateway plugin for Saman Bank
 *
 * @CommercePaymentGateway(
 *   id = "commerce_irpaymentpack_saman",
 *   label = @Translation("IRPaymentPack: Saman"),
 *   display_label = @Translation("Saman Bank"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect\SamanRedirect",
 *   },
 *   modes = {"Live"},
 *   payment_method_types = {"credit_card"},
 * )
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class SamanGateway extends OffsitePaymentGatewayBase {

  /**
   * URL to webservice WSDL
   *
   * There are different URLs, I don't know which one is preferred.
   * https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL
   * https://acquirer.samanepay.com/payments/referencepayment.asmx?WSDL
   * https://verify.sep.ir/Payments/ReferencePayment.asmx?WSDL
   */
  public const WSDL_URL = 'https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['saman_merchant_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Code'),
      '#default_value' => $this->configuration['saman_merchant_code'] ?? '',
      '#description' => $this->t('The merchant code is provided by Saman Bank.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      // Save configuration
      $this->configuration['saman_merchant_code'] = $values['saman_merchant_code'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $merchant_code = $this->configuration['saman_merchant_code'] ?? '';

    // If the configuration parameters are empty, display an error.
    if (empty($merchant_code)) {
      $msg = $this->t('Saman Bank is not configured properly. Please contact site administrator.');
      $this->messenger()->addError($msg);

      throw new InvalidRequestException($msg);
    }

    // load the previously saved payment
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->loadByProperties([
      'order_id'     => $order->id(),
      'state'        => 'authorization',
    ]);
    // The user may have a few payments (i.e. previous payment tries have been failed)
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = end($payment);
    if (!$payment) {
      // Could not find the payment.
      throw new InvalidRequestException($this->t('Could not find the payment record.'));
    }

    $res_num = $request->request->get('ResNum');
    $ref_num = $request->request->get('RefNum');
    $state   = $request->request->get('State');

    if ($state == 'OK') {
      $saman = new \SoapClient(self::WSDL_URL);
      $verify_result = $saman->VerifyTransaction($ref_num, $merchant_code);
      if ($verify_result > 0) {
        $payment->setState('completed');
        $payment->setRemoteId($res_num);
        $payment->setRemoteState($ref_num);
        $payment->setAuthorizedTime($this->time->getRequestTime());
        $payment->save();
        $this->messenger()->addStatus($this->t('Payment was successful.'));
      }
      else {
        $msg = $this->t('Transaction verification failed. Status: @status', ['@status' => $verify_result]);
        $this->messenger()->addError($msg);
        throw new PaymentGatewayException($msg);
      }
    }
    else {
      $msg = 'Transaction not successful or cancelled by user.';
      $this->messenger()->addError($this->t($msg));
      throw new PaymentGatewayException($msg);
    }
  }
}
