<?php

namespace Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Pasargad\Pasargad;
use Symfony\Component\HttpFoundation\Request;

/**
 * The commerce payment gateway plugin for Pasargad Bank
 *
 * @CommercePaymentGateway(
 *   id = "commerce_irpaymentpack_pasargad",
 *   label = @Translation("IRPaymentPack: Pasargad"),
 *   display_label = @Translation("Pasargad Bank"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect\PasargadRedirect",
 *   },
 *   modes = {"Live"},
 *   payment_method_types = {"credit_card"},
 * )
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class PasargadGateway extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // check if the library is installed.
    if (!static::checkLibraryInstalled()) {
      $form['error'] = [
        '#markup' => new FormattableMarkup(
          '<p style="color: red; font-weight: bold;">' .
          $this->t('WARNING: This gateway requires the "pepco-api/php-rest-sdk" composer package. Please install it first.') .
          '</p>',
          [],
        ),
      ];

      return $form;
    }

    $form['pasargad_merchant_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Code'),
      '#default_value' => $this->configuration['pasargad_merchant_code'] ?? '',
      '#description' => $this->t('The merchant code that you have got from the Bank.'),
      '#required' => TRUE,
    ];

    $form['pasargad_terminal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terminal Code'),
      '#default_value' => $this->configuration['pasargad_terminal_code'] ?? '',
      '#required' => TRUE,
      '#description' => $this->t('The terminal code that you have got from the Bank.'),
    ];

    $form['pasargad_certificate_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Certificate File'),
      '#description' => $this->t('Please upload the certificate.xml file that you\'ve got from the bank.'),
      '#upload_location' => 'private://commerce_irpaymentpack',
      '#multiple' => FALSE,
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => ['xml'],
      ],
    ];
    if (!empty($this->configuration['pasargad_certificate_file'])) {
      $form['pasargad_certificate_file']['#default_value'] = [$this->configuration['pasargad_certificate_file']];
    }

    $form['pasargad_order_base_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Base Order ID'),
      '#default_value' => $this->configuration['pasargad_order_base_id'] ?? 0,
      '#required' => TRUE,
      '#min' => 0,
      '#description' => $this->t('This number will be added to your shop\'s order id, to create the @bank remote Order ID.' .
        ' Note that the @bank requires a forever-unique order ID per gateway.', ['@bank' => $this->t('PasargadBank')]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // If the required package is not installed, prevent creation of the gateway.
    if (!static::checkLibraryInstalled()) {
      $form_state->setErrorByName('dummy', $this->t('WARNING: This gateway requires the "pepco-api/php-rest-sdk" composer package. Please install it first.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      // Save configuration
      $this->configuration['pasargad_merchant_code']    = $values['pasargad_merchant_code'];
      $this->configuration['pasargad_terminal_code']    = $values['pasargad_terminal_code'];
      $this->configuration['pasargad_order_base_id']    = $values['pasargad_order_base_id'];
      $this->configuration['pasargad_certificate_file'] = $values['pasargad_certificate_file'][0] ?? FALSE;

      if ($this->configuration['pasargad_certificate_file']) {
        /** @var \Drupal\file\FileInterface $certificate_file */
        $certificate_file = File::load($this->configuration['pasargad_certificate_file']);
        $certificate_file->setPermanent();
        $certificate_file->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $merchant_code    = $this->configuration['pasargad_merchant_code']    ?? '';
    $terminal_code    = $this->configuration['pasargad_terminal_code']    ?? '';
    $order_base_id    = $this->configuration['pasargad_order_base_id']    ?? '';
    $certificate_fid  = $this->configuration['pasargad_certificate_file'] ?? FALSE;
    $certificate_file = FALSE;
    if ($certificate_fid) {
      $certificate_file = File::load($certificate_fid);
    }

    // If the configuration parameters are empty, display an error.
    if (empty($merchant_code) || empty($terminal_code) || !($certificate_file instanceof FileInterface)) {
      $this->messenger()->addError($this->t('Bank is not configured properly. Please contact site administrator.'));

      throw new InvalidRequestException($this->t("Bank is not configured properly. Please contact site administrator."));
    }

    // calculate the remote order id. PasargadBank always requires a unique ID (forever-unique per gateway)
    $remote_order_id = $order_base_id + $order->id();

    // get amount
    $amount_rials = (int)$order->getTotalPrice()->getNumber();
    // convert TMN to IRR (If the currency code is set to TMN)
    if ($order->getTotalPrice()->getCurrencyCode() == 'TMN') {
      $amount_rials *= 10;
    }

    // load the previously saved payment
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->loadByProperties([
      'order_id'     => $order->id(),
      'state'        => 'authorization',
    ]);
    // The user may have a few payments (i.e. previous payment tries have been failed)
    $payment = end($payment);
    if (!$payment) {
      // Could not find the payment.
      throw new InvalidRequestException($this->t('Could not find the payment record.'));
    }

    // read received parameters
    $invoice_number    = $request->query->get('iN', '');
    $invoice_date      = $request->query->get('iD', '');
    $transaction_refid = $request->query->get('tref', '');

    try {
      // check transaction
      $pasargad = new Pasargad($merchant_code, $terminal_code, '', $certificate_file->getFileUri());
      $pasargad->setTransactionReferenceId($transaction_refid);
      $pasargad->setInvoiceNumber($invoice_number);
      $pasargad->setInvoiceDate($invoice_date);
      $check_result = $pasargad->checkTransaction();

      if (empty($check_result['IsSuccess'])) {
        $msg = $check_result['Message'] ?? $this->t('Error in payment gateway');
        $this->messenger()->addError($msg);
        throw new PaymentGatewayException($msg);
      }

      // verify transaction
      $pasargad = new Pasargad($merchant_code, $terminal_code, '', $certificate_file->getFileUri());
      $pasargad->setAmount($amount_rials);
      $pasargad->setInvoiceNumber($invoice_number);
      $pasargad->setInvoiceDate($invoice_date);
      $verify_result = $pasargad->verifyPayment();

      if (empty($verify_result['IsSuccess'])) {
        $msg = $verify_result['Message'] ?? $this->t('Error in payment gateway');
        $this->messenger()->addError($msg);
        throw new PaymentGatewayException($msg);
      }

      $payment->setState('completed');
      $payment->setRemoteId($request->request->get('SaleReferenceId'));
      $payment->save();
      $this->messenger()->addStatus($this->t('Payment was successful.'));
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
      watchdog_exception('commerce_irpaymentpack', $e);
    }
  }

  /**
   *
   */
  public static function checkLibraryInstalled(): bool {
    return class_exists('\Pasargad\Pasargad');
  }
}
