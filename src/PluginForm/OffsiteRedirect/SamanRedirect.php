<?php

namespace Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class is used to build the offsite redirect form for Saman Bank gateway
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class SamanRedirect extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The gateway redirect URL.
   */
  public const GATEWAY_URL = 'https://sep.shaparak.ir/payment.aspx';

  /**
   * request token URL
   */
  public const REQUEST_TOKEN = 'https://sep.shaparak.ir/payments/initpayment.asmx?wsdl';

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $paymentStorage;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerFactory;

  /**
   * constructor
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $logger_factory) {
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory->get('saman_gateway');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    // get amount
    $amount_rial = (int)$payment->getAmount()->getNumber();
    // convert Toman to Rial
    if ($payment->getAmount()->getCurrencyCode() == 'TMN') {
      $amount_rial *= 10;
    }

    // load gateway configuration
    $gateway_config = $payment->getPaymentGateway()->getPlugin()->getConfiguration();
    $merchant_code  = $gateway_config['saman_merchant_code'] ?? '';

    // If the configuration parameters are empty, display an error.
    if (empty($merchant_code)) {
      $this->messenger->addError($this->t('Saman Bank is not configured properly. Please contact site administrator.'));

      return $form;
    }

    // Create a new payment but with state 'Authorization' not completed.
    // On payment return, if everything is ok, the state of this new payment will be converted to 'Completed'.
    $new_payment = $this->paymentStorage->create([
      'state'           => 'authorization',
      'amount'          => $order->getTotalPrice(),
      'payment_gateway' => $payment->getPaymentGatewayId(),
      'order_id'        => $order->id(),
    ]);
    $new_payment->save();

    // request token
    $client = new \SoapClient(static::REQUEST_TOKEN);
    $token = $client->RequestToken($merchant_code, $order->id(), $amount_rial);

    $data = [
      'token'       => $token,
      'RedirectURL' => Url::fromUserInput('/saman-redirect.php', [
        'absolute' => TRUE,
        'https'    => TRUE,
        'query' => [
          'return' => $form['#return_url'],
        ],
      ])->toString(),
    ];

    return $this->buildRedirectForm($form, $form_state, static::GATEWAY_URL, $data, parent::REDIRECT_POST);
  }

}
