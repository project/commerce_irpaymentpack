<?php

namespace Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect;

use Drupal\commerce_irpaymentpack\Banks\MellatBank;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class is used to build the offsite redirect form for Mellat Bank
 *
 * @see \Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway\MellatGateway
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class MellatRedirect extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $paymentStorage;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerFactory;

  /**
   * constructor
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $logger_factory) {
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory->get('mellat_gateway');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    $amount_rials = (int)$payment->getAmount()->getNumber();

    // convert TMN to IRR (If the currency code is set to TMN)
    if ($payment->getAmount()->getCurrencyCode() == 'TMN') {
      $amount_rials *= 10;
    }

    // load gateway configuration
    $gateway_config = $payment->getPaymentGateway()->getPlugin()->getConfiguration();
    $terminal_id    = $gateway_config['mellat_terminal_id']   ?? '';
    $username       = $gateway_config['mellat_username']      ?? '';
    $password       = $gateway_config['mellat_password']      ?? '';
    $order_base_id  = $gateway_config['mellat_order_base_id'] ?? 0;

    // If the configuration parameters are empty, display an error.
    if (empty($terminal_id) || empty($username) || empty($password)) {
      $this->messenger->addError($this->t('Bank is not configured properly. Please contact site administrator.'));

      return $form;
    }

    // calculate the remote order id. MellatBank always requires a unique ID (forever-unique per gateway)
    $remote_order_id = $order_base_id + $order->id();

    $mellat = new MellatBank($terminal_id, $username, $password, $remote_order_id);
    $description = 'Order #' . $order->id();
    $result = $mellat->payRequest($amount_rials, $form['#return_url'], 0, $description);

    if ($result && $result['success']) {
      // Create a new payment but with state 'Authorization' not completed.
      // On payment return, if everything is ok, the state of this new payment will be converted to 'Completed'.
      $new_payment = $this->paymentStorage->create([
        'state'           => 'authorization',
        'amount'          => $order->getTotalPrice(),
        'payment_gateway' => $payment->getPaymentGatewayId(),
        'order_id'        => $order->id(),
        'remote_state'    => $result['refId'],
      ]);
      $new_payment->save();

      return $this->buildRedirectForm($form, $form_state, $mellat->getGateURL(), ['RefId' => $result['refId']], parent::REDIRECT_POST);
    }
    else {
      // Error during payRequest()
      $this->messenger->addError($this->t('Cannot initialize the payment.'));
      $this->loggerFactory->warning('Cannot initialize payment. Error code: @code. Meaning: @meaning. Order ID: @id', [
        '@code'    => $result['resCode'] ?? 'N/A',
        '@meaning' => isset($result['resCode']) ? $mellat->errorTranslate($result['resCode']) : 'N/A',
        '@id'      => $order->id()
      ]);

      return $form;
    }
  }

}
