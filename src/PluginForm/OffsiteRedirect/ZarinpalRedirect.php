<?php

namespace Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect;

use Drupal\commerce_irpaymentpack\Banks\Zarinpal;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class is used to build the offsite redirect form for Zarinpal gateway
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class ZarinpalRedirect extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $paymentStorage;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerFactory;

  /**
   * constructor
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $logger_factory) {
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory->get('zarinpal_gateway');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    // get amount
    $amount_toman = (int)$payment->getAmount()->getNumber();
    // convert Rial to Toman
    if ($payment->getAmount()->getCurrencyCode() == 'IRR') {
      $amount_toman /= 10;
    }

    // load gateway configuration
    $gateway_config = $payment->getPaymentGateway()->getPlugin()->getConfiguration();
    $merchant_code  = $gateway_config['zarinapl_merchant_code'] ?? '';

    // If the configuration parameters are empty, display an error.
    if (empty($merchant_code)) {
      $this->messenger->addError($this->t('Zarinpal is not configured properly. Please contact site administrator.'));

      return $form;
    }

    try {
      $zarinpal = new Zarinpal($merchant_code, $amount_toman);
      $zarinpal->setSandbox($gateway_config['mode'] == 'test');
      $authority = $zarinpal->paymentRequest($order->getStore()->label(), $form['#return_url'], $order->getEmail());

      // Create a new payment but with state 'Authorization' not completed.
      // On payment return, if everything is ok, the state of this new payment will be converted to 'Completed'.
      $new_payment = $this->paymentStorage->create([
        'state'           => 'authorization',
        'amount'          => $order->getTotalPrice(),
        'payment_gateway' => $payment->getPaymentGatewayId(),
        'order_id'        => $order->id(),
        'remote_state'    => $authority,
      ]);
      $new_payment->save();

      return $this->buildRedirectForm($form, $form_state, $zarinpal->buildGateURL($authority), [], parent::REDIRECT_POST);
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Cannot initialize the payment.'));
      $this->messenger->addError($e->getMessage());
      $this->loggerFactory->warning('Cannot initialize payment. Order ID: @id. Error: @error', [
        '@id'    => $order->id(),
        '@error' => $e->getMessage(),
      ]);

      return $form;
    }
  }

}
