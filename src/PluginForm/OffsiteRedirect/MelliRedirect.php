<?php

namespace Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect;

use Drupal\commerce_irpaymentpack\Banks\MelliBank;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class is used to build the offsite redirect form for Melli Bank
 *
 * @see \Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway\MelliGateway
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class MelliRedirect extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The URL for requesting payments.
   */
  public const PAY_REQUEST_URL = 'https://sadad.shaparak.ir/api/v0/Request/PaymentRequest';

  /**
   * The gateway URL (Should add ?Token=<token>)
   */
  public const GATEWAY_URL = 'https://sadad.shaparak.ir/Purchase';

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $paymentStorage;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerFactory;

  /**
   * constructor
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $logger_factory) {
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory->get('melli_gateway');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    $amount_rials = (int)$payment->getAmount()->getNumber();

    // convert TMN to IRR (If the currency code is set to TMN)
    if ($payment->getAmount()->getCurrencyCode() == 'TMN') {
      $amount_rials *= 10;
    }

    // load gateway configuration
    $gateway_config = $payment->getPaymentGateway()->getPlugin()->getConfiguration();
    $merchant_id    = $gateway_config['melli_merchant_id']   ?? '';
    $terminal_id    = $gateway_config['melli_terminal_id']   ?? '';
    $terminal_key   = $gateway_config['melli_terminal_key']  ?? '';
    $order_base_id  = $gateway_config['melli_order_base_id'] ?? 0;

    // If the configuration parameters are empty, display an error.
    if (empty($merchant_id) || empty($terminal_id) || empty($terminal_key)) {
      $this->messenger->addError($this->t('Bank is not configured properly. Please contact site administrator.'));

      return $form;
    }

    // calculate the remote order id. MellatBank always requires a unique ID (forever-unique per gateway)
    $remote_order_id = $order_base_id + $order->id();

    $data = [
      'TerminalId'    => $terminal_id,
      'MerchantId'    => $merchant_id,
      'Amount'        => $amount_rials,
      'SignData'      => MelliBank::encryptPKCS7("{$terminal_id};{$remote_order_id};{$amount_rials}", $terminal_key),
      'ReturnUrl'     => $form['#return_url'],
      'LocalDateTime' => date("m/d/Y g:i:s a"),
      'OrderId'       => $remote_order_id,
    ];

    try {
      $result = MelliBank::callAPI(self::PAY_REQUEST_URL, $data);
      if ($result['ResCode'] == 0) {

        // Create a new payment but with state 'Authorization' not completed.
        // On payment return, if everything is ok, the state of this new payment will be converted to 'Completed'.
        $new_payment = $this->paymentStorage->create([
          'state'           => 'authorization',
          'amount'          => $order->getTotalPrice(),
          'payment_gateway' => $payment->getPaymentGatewayId(),
          'order_id'        => $order->id(),
          'remote_state'    => $result['Token'],
        ]);
        $new_payment->save();

        return $this->buildRedirectForm($form, $form_state, self::GATEWAY_URL, ['Token' => $result['Token']], parent::REDIRECT_GET);
      }
      else {
        $this->messenger->addError($this->t('Cannot initialize the payment.'));
        $this->loggerFactory->warning('Cannot initialize payment. Order ID: @id. Description: @desc', [
          '@id'   => $order->id(),
          '@desc' => $result['Description'] ?? 'N/A',
        ]);

        return $form;
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Cannot initialize the payment.'));
      watchdog_exception('commerce_irpaymentpack', $e);
      $this->loggerFactory->warning('Cannot initialize payment. Order ID: @id. Description: @desc', [
        '@id'   => $order->id(),
        '@desc' => $e->getMessage(),
      ]);

      return $form;
    }
  }

}
