<?php

namespace Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect;

use Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway\SaderatGateway;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class is used to build the offsite redirect form for Saderat Bank
 *
 * @see \Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway\SaderatGateway
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class SaderatRedirect extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * Rest endpoint URL for getting token
   */
  protected const GET_TOKEN_REST_ENDPOINT = 'https://sepehr.shaparak.ir:8081/V1/PeymentApi/GetToken';

  /**
   * The payment gateway URL
   */
  protected const PAYMENT_GATEWAY_URL = 'https://sepehr.shaparak.ir:8080/pay';

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $paymentStorage;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerFactory;

  /**
   * constructor
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $logger_factory) {
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory->get('saderat_gateway');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    $amount_rials = (int)$payment->getAmount()->getNumber();

    // convert TMN to IRR (If the currency code is set to TMN)
    if ($payment->getAmount()->getCurrencyCode() == 'TMN') {
      $amount_rials *= 10;
    }

    // load gateway configuration
    $gateway_config = $payment->getPaymentGateway()->getPlugin()->getConfiguration();
    $terminal_id    = $gateway_config['saderat_terminal_id']   ?? '';
    $order_base_id  = $gateway_config['saderat_order_base_id'] ?? 0;

    // If the configuration parameters are empty, display an error.
    if (empty($terminal_id)) {
      $this->messenger->addError($this->t('Bank is not configured properly. Please contact site administrator.'));

      return $form;
    }

    // calculate the remote order id. Saderat Bank always requires a unique ID (forever-unique per gateway)
    $remote_order_id = $order_base_id + $order->id();

    // Post Data
    $data = [
      'Amount'      => $amount_rials,
      'callbackURL' => $form['#return_url'],
      'invoiceID'   => $remote_order_id,
      'terminalID'  => $terminal_id,
      'payload'=> ''
    ];

    // Get Token
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, self::GET_TOKEN_REST_ENDPOINT);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    $curl_err = curl_error($ch);
    curl_close($ch);

    if (empty($result)) {
      $this->messenger->addError($this->t('Cannot connect to the bank.'));
      $this->loggerFactory->warning('Cannot connect to the bank. Error: @err. Order ID: @id', [
        '@err' => $curl_err,
        '@id'  => $order->id()
      ]);

      return $form;
    }

    $token_arr = Json::decode($result);
    $token  = $token_arr['Accesstoken'] ?? '';
    $status = $token_arr['Status'] ?? NULL;

    if (!empty($token) && $status == 0) {
      // Create a new payment but with state 'Authorization' not completed.
      // On payment return, if everything is ok, the state of this new payment will be converted to 'Completed'.
      $new_payment = $this->paymentStorage->create([
        'state'           => 'authorization',
        'amount'          => $order->getTotalPrice(),
        'payment_gateway' => $payment->getPaymentGatewayId(),
        'order_id'        => $order->id(),
        'remote_state'    => $token,
      ]);
      $new_payment->save();

      return $this->buildRedirectForm($form, $form_state, self::PAYMENT_GATEWAY_URL, [
        'TerminalID' => $terminal_id,
        'token'      => $token,
        'getMethod'  => 0, // 0 means that when returning from Bank, it should be redirected using a POST method. 1 means GET
      ], parent::REDIRECT_POST);
    }
    else {
      // Error during payRequest()
      $this->messenger->addError($this->t('Cannot initialize the payment.'));
      $this->loggerFactory->warning('Cannot initialize payment. Error code: @code. Meaning: @meaning. Order ID: @id', [
        '@code'    => $status ?? 'N/A',
        '@meaning' => SaderatGateway::translateErrorCode($status),
        '@id'      => $order->id()
      ]);

      return $form;
    }
  }

}
