<?php

namespace Drupal\commerce_irpaymentpack\PluginForm\OffsiteRedirect;

use Drupal\commerce_irpaymentpack\Plugin\Commerce\PaymentGateway\PasargadGateway;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Pasargad\Pasargad;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class is used to build the offsite redirect form for Pasargad Bank
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class PasargadRedirect extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $paymentStorage;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerFactory;

  /**
   * constructor
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              MessengerInterface $messenger,
                              LoggerChannelFactoryInterface $logger_factory) {
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory->get('pasargad_gateway');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    $amount_rials = (int)$payment->getAmount()->getNumber();

    // convert TMN to IRR (If the currency code is set to TMN)
    if ($payment->getAmount()->getCurrencyCode() == 'TMN') {
      $amount_rials *= 10;
    }

    // load gateway configuration
    $gateway_config   = $payment->getPaymentGateway()->getPlugin()->getConfiguration();
    $merchant_code    = $gateway_config['pasargad_merchant_code']    ?? '';
    $terminal_code    = $gateway_config['pasargad_terminal_code']    ?? '';
    $order_base_id    = $gateway_config['pasargad_order_base_id']    ?? 0;
    $certificate_fid  = $gateway_config['pasargad_certificate_file'] ?? FALSE;
    $certificate_file = FALSE;
    if ($certificate_fid) {
      $certificate_file = File::load($certificate_fid);
    }

    // If the configuration parameters are empty, display an error.
    // also check for library requirement.
    if (empty($merchant_code) || empty($terminal_code) || !($certificate_file instanceof FileInterface) || !PasargadGateway::checkLibraryInstalled()) {
      $this->messenger->addError($this->t('Bank is not configured properly. Please contact site administrator.'));

      return $form;
    }

    // calculate the remote order id. PasargadBank always requires a unique ID (forever-unique per gateway)
    $remote_order_id = $order_base_id + $order->id();

    try {
      $pasargad = new Pasargad($merchant_code, $terminal_code, $form['#return_url'], $certificate_file->getFileUri());
      $pasargad->setAmount($amount_rials);
      $pasargad->setInvoiceNumber($remote_order_id);

      // set Invoice Date with below format (Y/m/d H:i:s)
      $pasargad->setInvoiceDate(date('Y/m/d H:i:s', $order->getCreatedTime()));

      // set mobile and email address
      //$pasargad->setMobile("09391234567");
      $pasargad->setEmail($order->getEmail());

      // Create a new payment but with state 'Authorization' not completed.
      // On payment return, if everything is ok, the state of this new payment will be converted to 'Completed'.
      $new_payment = $this->paymentStorage->create([
        'state'           => 'authorization',
        'amount'          => $order->getTotalPrice(),
        'payment_gateway' => $payment->getPaymentGatewayId(),
        'order_id'        => $order->id(),
        'remote_state'    => $pasargad->getToken(),
      ]);
      $new_payment->save();

      return $this->buildRedirectForm($form, $form_state, $pasargad->redirect(), [], parent::REDIRECT_POST);
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Cannot initialize the payment.'));
      $this->messenger->addError($e->getMessage());
      $this->loggerFactory->warning('Cannot initialize payment. Order ID: @id. Error: @error', [
        '@id'    => $order->id(),
        '@error' => $e->getMessage(),
      ]);

      return $form;
    }
  }

}
