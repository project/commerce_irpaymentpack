<?php

namespace Drupal\commerce_irpaymentpack\Banks;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Handles Zarinpal Gateway transactions
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class Zarinpal {

  use StringTranslationTrait;

  /**
   * @var string The Merchant ID
   */
  protected string $merchantID;

  /**
   * @var int Amount in Toman
   */
  protected int $amount;

  /**
   * Zarinpal webgate WSDL address (live mode)
   */
  protected const WEBGATE_WSDL_LIVE = 'https://www.zarinpal.com/pg/services/WebGate/wsdl';

  /**
   * Zarinpal webgate WSDL address (Sandbox mode)
   */
  protected const WEBGATE_WSDL_SANDBOX = 'https://sandbox.zarinpal.com/pg/services/WebGate/wsdl';

  /**
   * @var string Zarinpal Webgate address prefix
   */
  protected const WEBGATE = 'https://www.zarinpal.com/pg/StartPay/';

  /**
   * @var bool Whether we should use the sandbox mode
   */
  protected bool $sandbox = FALSE;

  /**
   * Class Constructor
   *
   * @param string $merchantID
   *   Merchant ID
   * @param int $amount
   *   Amount In Toman
   */
  function __construct(string $merchantID, int $amount) {
    $this->merchantID = $merchantID;
    $this->amount = $amount;
  }

  /**
   * check whether we are using the sandbox mode.
   */
  public function getSandbox(): bool {
    return $this->sandbox;
  }

  /**
   * Set the sandbox flag.
   */
  public function setSandbox(bool $sandbox = TRUE) {
    $this->sandbox = $sandbox;
  }

  /**
   * Request payment.
   *
   * @param string $description
   *   Description of Payment
   * @param string $callbackURL
   *   Callback url of the Shopping site
   * @param string $email
   *   Optional email of customer
   * @param string $mobile
   *   Optional mobile number of the customer
   *
   * @throws ZarinpalException
   * @throws \SoapFault
   *
   * @return string Authority number
   */
  public function paymentRequest(string $description, string $callbackURL, string $email= '', string $mobile = ''): string {
    $wsdl = $this->getSandbox() ? static::WEBGATE_WSDL_SANDBOX : static::WEBGATE_WSDL_LIVE;
    $args = [
      'MerchantID'  => $this->merchantID,
      'Amount'      => $this->amount,
      'Description' => $description,
      'Email'       => $email,
      'Mobile'      => $mobile,
      'CallbackURL' => $callbackURL
    ];

    $client = new \SoapClient($wsdl, ['encoding' => 'UTF-8']);

    $result = $client->PaymentRequest($args);

    if (($result->Status == 100) && (isset($result->Authority)) && (strlen($result->Authority) == 36)) {
      $authority = $result->Authority;
    }
    else {
      throw new ZarinpalException($this->t('Can not request payment. @msg', [
        '@msg' => $this->translateErrorCode($result->Status),
      ]));
    }

    return $authority;
  }

  /**
   * Verify transaction
   *
   * @param ?string $authority
   *   The Authority number which is passed by Zarinpal to the callback page as QueryString
   *   If Null provided, it will be got from $_GET
   *
   * @throws ZarinpalException
   * @throws ZarinpalExceptionNok
   * @throws ZarinpalExceptionVerification
   * @throws \SoapFault
   *
   * @return string Reference ID of transaction if it is successful.
   */
  public function verifyTransaction(?string $authority = NULL): string {
    // Validate $Authority and $Status (passed by Zarinpal via QueryString in callback url)
    if (is_null($authority)) {
      $status = $_GET['Status'] ?? NULL;
      if ($status == 'OK') {
        if (isset($_GET['Authority'])) {
          $authority = $_GET['Authority'];
        }
        else {
          throw new ZarinpalException($this->t('Error: $_GET[Authority] not available'));
        }
      }
      elseif ($status == 'NOK') {
        // In Sample PHP Code: Transaction canceled by user.
        throw new ZarinpalExceptionNok();
      }
      else {
        throw new ZarinpalException($this->t('Error: $_GET[Status] invalid'));
      }
    }

    // Validated. Now Process verification
    $wsdl = $this->getSandbox() ? static::WEBGATE_WSDL_SANDBOX : static::WEBGATE_WSDL_LIVE;
    $args = [
      'MerchantID' => $this->merchantID,
      'Amount' 		 => $this->amount,
      'Authority'  => $authority,
    ];

    $client = new \SoapClient($wsdl, ['encoding' => 'UTF-8']);
    $res = $client->PaymentVerification($args);

    if (isset($res->Status) && ($res->Status == 100)) {
      $refID = $res->RefID;
    }
    else {
      throw new ZarinpalExceptionVerification($this->t('Transaction verification failed. RefID: @refId, Message: @message', [
        '@refId'   => $res->RefID,
        '@message' => $this->translateErrorCode($res->Status),
      ]));
    }

    return $refID;
  }

  /**
   * Build WebGate or ZarinGate URL. The user should be redirected to this address for payment
   *
   * @param string $authority The authority code returned by paymentRequest() method
   * @param string $type Type of Gate. Either 'webgate' or 'zaringate'. defaults to 'webgate'
   *
   * @return string Gate url
   *
   * @throws ZarinpalException
   */
  public function buildGateURL(string $authority, string $type = 'webgate'): string {
    switch ($type) {
      case 'webgate':
        $url = static::WEBGATE . $authority;
        break;
      case 'zaringate':
        $url = static::WEBGATE . $authority . '/ZarinGate';
        break;
      default:
        throw new ZarinpalException('Invalid gate type: should be either "webgate" or "zaringate"');
    }

    return $url;
  }

  /**
   * Translates zarinpal error codes
   *
   * @param int $code The error code
   *
   * @return string Translated message
   */
  public function translateErrorCode(int $code): string {
    switch ($code) {
      case -1:
        $msg = $this->t('Incomplete data.');
        break;
      case -2:
        $msg = $this->t('Invalid IP or MerchantID.');
        break;
      case -3:
        $msg = $this->t('Cannot pay this amount due to restrictions of the Shapark system.');
        break;
      case -4:
        $msg = $this->t('Merchant\'s verify-level is below the accepted silver level.');
        break;
      case -11:
        $msg = $this->t('Specified request was not found.');
        break;
      case -12:
        $msg = $this->t('Can\'t edit request.');
        break;
      case -21:
        $msg = $this->t('No financial actions found for this transaction.');
        break;
      case -22:
        $msg = $this->t('Transaction failed.');
        break;
      case -33:
        $msg = $this->t('Transaction\'s amount does not match with the paid amount.');
        break;
      case -34:
        $msg = $this->t('Maximum transaction division exceeded (by quantity or amount).');
        break;
      case -40:
        $msg = $this->t('Access to requested method is denied.');
        break;
      case -41:
        $msg = $this->t('Invalid data sent to AdditionalData() method.');
        break;
      case -42:
        $msg = $this->t('Payment authority lifetime should be between 30 minutes and 45 days.');
        break;
      case -54:
        $msg = $this->t('The specified request is archived.');
        break;
      case 100:
        $msg = $this->t('Action completed successfully');
        break;
      case 101:
        $msg = $this->t('Payment was successful but PaymentVerification() method is already done.');
        break;
      default:
        $msg = $this->t('Unknown error code.');
    }

    return $this->t("Error code: @code : @msg", [
      '@code' => $code,
      '@msg'  => $msg,
    ]);
  }
}
