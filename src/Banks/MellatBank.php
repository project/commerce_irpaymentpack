<?php

namespace Drupal\commerce_irpaymentpack\Banks;

/**
 * Provides integration with Mellat Bank (An Iranian Bank)
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */
class MellatBank {

  /**
   * The WSDL endpoint URL
   */
  protected const WSDL_ENDPOINT = 'https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl';

  /**
   * The action of payment form (gateway URL)
   */
  protected const GATE_URL = 'https://bpm.shaparak.ir/pgwchannel/startpay.mellat';

  /**
   * Namespace for soap calls
   */
  protected const SOAP_NAMESPACE = 'http://interfaces.core.sw.bps.com/';

  /**
   * @var string The Terminal ID given by Mellat Bank
   */
  protected string $terminalId;

  /**
   * @var string The username given by Mellat Bank
   */
  protected string $userName;

  /**
   * @var string The password given by Mellat Bank
   */
  protected string $userPassword;

  /**
   * @var int The unique order ID (required by mellat web service)
   */
  protected int $orderId;

  /**
   * @var object Holds the soap client object
   */
  protected $soap;

  /**
   * Holds error info
   */
  private array $err;

  /**
   * Constructor function
   *
   * @param string $terminalId
   * @param string $userName
   * @param string $userPassword
   * @param int $orderId
   *  The order id should be unique for all requests ever sent to mellat bank.
   *  Always use an increasing number.
   */
  public function __construct(string $terminalId, string $userName, string $userPassword, int $orderId) {
    $this->terminalId = $terminalId;
    $this->userName = $userName;
    $this->userPassword = $userPassword;
    $this->orderId = $orderId;

    $this->soap = new \SoapClient(self::WSDL_ENDPOINT);
  }

  /**
   * Request Payment
   *
   * @param int $amount
   *   The amount in Rials
   * @param $callBackUrl
   *   The absolute url to redirect user after coming back from bank.
   * @param int $payerId
   * @param string $additionalData
   * @param string $localDate
   * @param string $localTime
   *
   * @return ?array
   * array (
   *   success => bool,
   *   refId   => refId,
   *   resCode => resCode --- or False on fatal errors
   * )
   */
  public function payRequest(int $amount, string $callBackUrl, int $payerId = 0, string $additionalData = '', string $localDate = '', string $localTime = ''): ?array {
    // Populate default arguments
    if (empty($localDate)) {
      $localDate = date("Ymd");
    }
    if (empty($localTime)) {
      $localTime = date("His");
    }

    // Populate the parameters array
    $parameters = [
      'terminalId'     => $this->terminalId,
      'userName'       => $this->userName,
      'userPassword'   => $this->userPassword,
      'orderId'        => $this->orderId,
      'amount'         => $amount,
      'localDate'      => $localDate,
      'localTime'      => $localTime,
      'additionalData' => $additionalData,
      'callBackUrl'    => $callBackUrl,
      'payerId'        => $payerId,
    ];

    // Call web service
    try {
      $result = $this->soap->bpPayRequest($parameters, self::SOAP_NAMESPACE);
    }
    catch (\SoapFault $e) {
      $this->setError('soap', 'bpPayRequest: ' . $e->getMessage(), $e);

      return NULL;
    }

    // the result should be stdClass object with return property
    if (!isset($result->return)) {
      $this->setError('soap', 'bpPayRequest: soap returned invalid response.', $result);

      return NULL;
    }

    // Parse result
    $result_arr = explode(',', $result->return);

    // if success, the $result_arr should be an array of resCode and refId
    // on failure, the $result->return is an error number
    if (!is_array($result_arr)) {
      $this->setError('bank', 'bpPayRequest returned error', $result->return);

      return NULL;
    }

    $resCode = $result_arr[0] ?? NULL;
    $refId = $result_arr[1] ?? NULL;

    // Check if we are successful
    if (($resCode === '0') && (!empty($refId))) {
      return ['refId' => $refId, 'resCode' => $resCode, 'success' => TRUE];
    }
    else {
      $this->setError('bank', 'Error in calling bpPayRequest: Invalid result code.', $resCode);

      return ['refId' => $refId, 'resCode' => $resCode, 'success' => FALSE];
    }
  }

  /**
   * Get parameters sent from bank gateway (to the callback URL)
   *
   * If a single parameter is needed, pass the parameter name.
   */
  public function getCallBackParameters($param = '') {
    $params = [
      'refId'           => $_POST['RefId'] ?? '',
      'resCode'         => $_POST['ResCode'] ?? '',
      'saleOrderId'     => $_POST['SaleOrderId'] ?? '',
      'saleReferenceId' => $_POST['SaleReferenceId'] ?? '',
    ];

    if ($param) {
      return $params[$param] ?? FALSE;
    }
    else {
      return $params;
    }
  }

  /**
   * Verify Request.
   *
   * @return bool Whether verification was successful or not.
   */
  public function verifyRequest($saleOrderId, $saleReferenceId): bool {
    // Populate parameters.
    $parameters = [
      'terminalId'      => $this->terminalId,
      'userName'        => $this->userName,
      'userPassword'    => $this->userPassword,
      'orderId'         => $this->orderId,
      'saleOrderId'     => $saleOrderId,
      'saleReferenceId' => $saleReferenceId,
    ];

    // Call web service
    try {
      $verify_result = $this->soap->bpVerifyRequest($parameters, self::SOAP_NAMESPACE);
    }
    catch (\SoapFault $e) {
      $this->setError('soap', 'bpVerifyRequest: ' . $e->getMessage(), $e);

      return FALSE;
    }

    if (!isset($verify_result->return)) {
      $this->setError('soap', 'bpVerifyRequest: soap returned invalid response.', $verify_result);

      return FALSE;
    }

    // Parse result
    $result_arr = explode(',', $verify_result->return);

    // if success, the $result_arr should be an array
    if (!is_array($result_arr)) {
      $this->setError('bank', 'bpVerifyRequest returned error', $verify_result->return);

      return FALSE;
    }

    $resCode = $result_arr[0];

    if ($resCode !== '0') {
      // Second try: Inquiry
      try {
        $inquiry_result = $this->soap->bpInquiryRequest($parameters, self::SOAP_NAMESPACE);
      }
      catch (\SoapFault $e) {
        $this->setError('soap', 'bpInquiryRequest: ' . $e->getMessage(), $e);

        return FALSE;
      }

      if (!isset($inquiry_result->return)) {
        $this->setError('soap', 'bpInquiryRequest: soap returned invalid response.', $inquiry_result);

        return FALSE;
      }

      // Parse result
      $result_arr = explode(',', $inquiry_result->return);

      // if success, the $result_arr should be an array
      if (!is_array($result_arr)) {
        $this->setError('bank', 'bpInquiryRequest returned error', $inquiry_result->return);

        return FALSE;
      }

      $resCode = $result_arr[0];

      if ($resCode !== '0') {
        $this->setError('bank', 'bpInquiryRequest unsuccessful (After an unsuccessful bpVerifyRequest)', $resCode);

        // Final try: Reverse the payment (we can't verify it anyway)
        try {
          $this->soap->bpReversalRequest($parameters, self::SOAP_NAMESPACE);
        }
        catch (\SoapFault $e) {
          $this->setError('soap', 'bpReversalRequest: ' . $e->getMessage(), $e);

          return FALSE;
        }

        // we don't need to check the status of bpReversalRequest because
        // there's no further action that we can take on it.

        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Set Error message
   */
  protected function setError(string $type, string $message, $object): void {
    $this->err = [
      'type'    => $type,
      'message' => $message,
      'object'  => $object,
    ];
  }

  /**
   * Get error
   */
  public function getError(): ?array {
    return $this->err ?? NULL;
  }

  /**
   * Translate MellatBank error codes to plain English.
   */
  public function errorTranslate(string $error_code): string {
    $codes = array(
      '-1'  => 'The payment has completed but not settled.',
      '-2'  => 'Can not call bpSettleRequest internally.',
      '-3'  => 'Can not call bpVerifyRequest internally.',
      '-4'  => 'Service can not initiated internally.',
      '-5'  => 'Remote ID associated with transaction and RefId are not equal.',
      '-6'  => 'document has already been sent back',
      '-7'  => 'digital receipt is empty',
      '-8'  => 'input length is bigger than allowed',
      '-9'  => 'invalid characters in returned payment',
      '-10' => 'digital receipt contains invalid characters',
      '-11' => 'input length is smaller than allowed',
      '-12' => 'returned payment is negative',
      '-13' => 'returned payment of partial payment is more than the amount of not returned digital receipt',
      '-14' => 'such transaction is not defined',
      '-15' => 'returned payment is entered in decimal',
      '-16' => 'internal system error',
      '-17' => 'to refund a transaction done by non-mellat bank cards',
      '-18' => 'seller IP address is invalid',
      '-19' => 'can not call service soap is invalid',
      '0'   => 'The payment has completed and settled.',
      '11'  => 'Incorrect account number.',
      '12'  => 'Inadequate balance.',
      '13'  => 'Incorrect password.',
      '14'  => 'Too many incorrect password retries.',
      '15'  => 'Incorrect card.',
      '16'  => 'Withdrawal times over limit.',
      '17'  => 'Transaction terminated by user.',
      '18'  => 'Card expiration date is past.',
      '19'  => 'Withdrawal amount is excessive',
      '111' => 'Invalid account provider.',
      '112' => 'Switch error in account provider.',
      '113' => 'No response from account provider.',
      '114' => 'Card owner is not allowed to perform this transaction.',
      '21'  => 'Invalid Merchant.',
      '23'  => 'Security error has occurred.',
      '24'  => 'Invalid store credentials.',
      '25'  => 'Invalid amount.',
      '31'  => 'Invalid response.',
      '32'  => 'Invalid input format.',
      '33'  => 'Invalid account.',
      '34'  => 'System error.',
      '35'  => 'Invalid date.',
      '41'  => 'Duplicate Order ID.',
      '42'  => 'Sale transaction not found.',
      '43'  => 'Transaction already verified.',
      '44'  => 'Verify transaction not found.',
      '45'  => 'Transaction already settled.',
      '46'  => 'Transaction is not settled.',
      '47'  => 'Settle transaction not found.',
      '48'  => 'Transaction reversed previously.',
      '49'  => 'Refund transaction not found.',
      '412' => 'Invalid receipt ID.',
      '413' => 'Invalid payment ID.',
      '414' => 'Invalid receipt diffuser.',
      '415' => 'Session has been expired.',
      '416' => 'Error in storing data.',
      '417' => 'Invalid payer ID.',
      '418' => 'Problem in defining user information.',
      '419' => 'Too many form submission retries.',
      '421' => 'Invalid IP.',
      '51'  => 'Duplicate transaction.',
      '54'  => 'Transaction not found.',
      '55'  => 'Invalid transaction.',
      '61'  => 'Error in payment.',
    );

    return $codes[$error_code] ?? 'Unrecognized error code.';
  }

  /**
   * Get Gateway URL
   */
  public function getGateURL(): string {
    return self::GATE_URL;
  }
}
