<?php

namespace Drupal\commerce_irpaymentpack\Banks;

/**
 * Contains helper functions for interacting with MelliBank API.
 *
 * TODO: Add error translation per Bank's document
 *
 * @see https://github.com/SadadPsp-Lab/PHP
 */
class MelliBank {

  /**
   * Helper function for signing.
   */
  public static function encryptPKCS7(string $str, string $key): string {
    $key = base64_decode($key);
    $ciphertext = openssl_encrypt($str, "DES-EDE3", $key, OPENSSL_RAW_DATA);

    return base64_encode($ciphertext);
  }

  /**
   * Helper function to call APIs
   *
   * @throws \Exception
   */
  public static function callAPI(string $url, ?array $data = NULL): ?array {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json; charset=utf-8'));
    curl_setopt($ch, CURLOPT_POST, 1);
    if ($data) {
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($ch);
    curl_close($ch);

    return !empty($result) ? \json_decode($result, TRUE) : NULL;
  }
}
