<!doctype html>
<?php

/**
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 *
 * Redirect back to commerce. but preserve POST parameters sent from Saman Bank
 * This script is indented to overcome SameSite cookie attribute issues of Chrome (and other browsers.)
 *
 * Instructions:
 * Copy this file to drupal installation folder, beside index.php
 */

$params = array(
  'ResNum'   => isset($_POST['ResNum']) ? $_POST['ResNum']  : '',
  'RefNum'   => isset($_POST['RefNum']) ? $_POST['RefNum']  : '',
  'State'    => isset($_POST['State'])  ? $_POST['State']   : '',
);

// build return path.
$return_path = $_GET['return'];

?>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>در حال پردازش بانکی...</title>

</head>
<body dir="rtl">
  <div class="container">
    <div class="row">
      <div class="col-12">

        <?php if (empty($params['ResNum']) || empty($params['RefNum']) || empty($params['State'])): ?>

          <h1>در درگاه پرداخت بانک خطایی رخ داده است.</h1>

        <?php else: ?>

          <h1>سفارش شما در حال پردازش است، لطفا پنجره مرورگر را نبندید.</h1>
          <hr style="margin-bottom: 2rem;" />
          <p>لطفا تا زمانی که پیام تکمیل سفارش را در سایت مشاهده کنید، پنجره مرورگر را نبندید.</p>
          <p>در صورتی که تا
          <span style="font-style: italic; color: #279d27;">چند لحظه دیگر</span>
          به سایت هدایت نشدید، روی دکمه تکمیل سفارش در زیر کلیک کنید.</p>
          <br>
          <form action="<?php print $_GET['return']; ?>" method="post" id="the_form">
            <input type="hidden" name="ResNum" value="<?php print $params['ResNum']; ?>" />
            <input type="hidden" name="RefNum" value="<?php print $params['RefNum']; ?>" />
            <input type="hidden" name="State"  value="<?php print $params['State']; ?>" />

            <input type="submit" value="تکمیل سفارش" class="btn btn-success btn-lg" />
          </form>

        <?php endif; ?>

      </div>
    </div>
  </div>

  <script src="/core/assets/vendor/jquery/jquery.min.js"></script>
  <script>
    (function($) {
      $(function() {
        $('#the_form').submit();
      });
    })(jQuery);
  </script>
</body>
</html>
